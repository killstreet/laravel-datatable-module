<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

trait dataTableEditor {

    public static function getTables() {
        //return all tables from database
        $tables = DB::select('SHOW TABLES');

        return $tables;
    }

    public static function getColumnsFromTable($table) {
        //return all columns from table
        return Schema::getColumnListing($table);
    }

    public static function getDataFromTable($table) {
        // return all data from table
        $arr = DB::table($table)
            ->get();

        return $arr;
    }

    public static function update($id, $table, $column, $value) {
        $data = DB::table($table)
            ->where('id', $id)
            ->update([$column => $value]);

        return 'success';
    }
}