<?php

namespace App\Http\Controllers;

use App\Traits\dataTableEditor;
use Illuminate\Http\Request;


class TableController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show ($table) {
        $columns = dataTableEditor::getColumnsFromTable($table);
        $datas = dataTableEditor::getDataFromTable($table);

//        dd($columns);

        return view('tables.show', compact('columns', 'table', 'datas'));
    }

    public function update(Request $request) {
        return dataTableEditor::update($request->input('id'), $request->input('table'), $request->input('column'), $request->input('data'));
    }
}
