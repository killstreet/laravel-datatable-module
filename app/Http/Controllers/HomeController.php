<?php

namespace App\Http\Controllers;

use App\Traits\dataTableEditor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        Artisan::call('custom:migrate');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tables = dataTableEditor::getTables();

        return view('home', compact('tables'));
    }
}
