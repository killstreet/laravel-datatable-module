@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" id="table-name">{{ $table }}</div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        @foreach($columns as $column)
                            <th>{{ $column }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($datas as $data)
                        <tr>
                            @foreach($columns as $column)
                                <td contenteditable="true" myid="{{ $data->id }}" class="editable table-{{ $table }} column-{{ $column }}">{{ $data->$column }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>

    <script>
        $(".editable").keydown(function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                //Enter key pressed, update the table
                classList = $(this)[0].classList;
                var newValue = $(this).text();
                var myId = $(this).attr('myid');

                console.log(newValue);

                var functionalList = [];

                for (var i = 0; i < classList.length; i++) {
                    if (classList[i] !== 'editable') {
                        console.log(classList[i]);
                        var data = classList[i].split('-');
                        console.log(data);
                        functionalList[data[0]] = data[1];
                    }
                }

                updateValue(functionalList['table'], functionalList['column'], newValue, myId);
            }
        });

        function updateValue(table, column, data, myId) {
            $.ajax({
                url: "update",
                data: {id: myId, table:  table, column: column, data: data, _token: $("meta[name='csrf-token']").attr("content")},
                method: "POST",
                success: function (result) {
                    if(result == "success") {
                        alert('success');
                    } else {
                        alert('error occured');
                    }
                }
            })
        }
    </script>
@endsection
