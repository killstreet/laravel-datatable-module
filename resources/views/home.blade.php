@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Table editor</div>
                <ul>
                    @foreach($tables as $table)
                        <li><a href="{{ route('table', ['table' => $table->Tables_in_test]) }}">{{ $table->Tables_in_test }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
